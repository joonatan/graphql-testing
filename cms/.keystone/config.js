"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// keystone.ts
var keystone_exports = {};
__export(keystone_exports, {
  default: () => keystone_default
});
module.exports = __toCommonJS(keystone_exports);
var import_core2 = require("@keystone-6/core");

// schema.ts
var import_core = require("@keystone-6/core");
var import_access = require("@keystone-6/core/access");
var import_fields = require("@keystone-6/core/fields");
var import_fields_document = require("@keystone-6/fields-document");
var lists = {
  User: (0, import_core.list)({
    // WARNING
    //   for this starter project, anyone can create, query, update and delete anything
    //   if you want to prevent random people on the internet from accessing your data,
    //   you can find out more at https://keystonejs.com/docs/guides/auth-and-access-control
    access: import_access.allowAll,
    // this is the fields for our User list
    fields: {
      // by adding isRequired, we enforce that every User should have a name
      //   if no name is provided, an error will be displayed
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      email: (0, import_fields.text)({
        validation: { isRequired: true },
        // by adding isIndexed: 'unique', we're saying that no user can have the same
        // email as another user - this may or may not be a good idea for your project
        isIndexed: "unique"
      }),
      password: (0, import_fields.password)({ validation: { isRequired: true } }),
      // we can use this field to see what Posts this User has authored
      //   more on that in the Post list below
      posts: (0, import_fields.relationship)({ ref: "Post.author", many: true }),
      createdAt: (0, import_fields.timestamp)({
        // this sets the timestamp to Date.now() when the user is first created
        defaultValue: { kind: "now" }
      })
    }
  }),
  Post: (0, import_core.list)({
    // WARNING
    //   for this starter project, anyone can create, query, update and delete anything
    //   if you want to prevent random people on the internet from accessing your data,
    //   you can find out more at https://keystonejs.com/docs/guides/auth-and-access-control
    access: import_access.allowAll,
    // this is the fields for our Post list
    fields: {
      title: (0, import_fields.text)({ validation: { isRequired: true } }),
      // the document field can be used for making rich editable content
      //   you can find out more at https://keystonejs.com/docs/guides/document-fields
      content: (0, import_fields_document.document)({
        formatting: true,
        layouts: [
          [1, 1],
          [1, 1, 1],
          [2, 1],
          [1, 2],
          [1, 2, 1]
        ],
        links: true,
        dividers: true
      }),
      // with this field, you can set a User as the author for a Post
      author: (0, import_fields.relationship)({
        // we could have used 'User', but then the relationship would only be 1-way
        ref: "User.posts",
        // this is some customisations for changing how this will look in the AdminUI
        ui: {
          displayMode: "cards",
          cardFields: ["name", "email"],
          inlineEdit: { fields: ["name", "email"] },
          linkToItem: true,
          inlineConnect: true
        },
        // a Post can only have one author
        //   this is the default, but we show it here for verbosity
        many: false
      }),
      // with this field, you can add some Tags to Posts
      tags: (0, import_fields.relationship)({
        // we could have used 'Tag', but then the relationship would only be 1-way
        ref: "Tag.posts",
        // a Post can have many Tags, not just one
        many: true,
        // this is some customisations for changing how this will look in the AdminUI
        ui: {
          displayMode: "cards",
          cardFields: ["name"],
          inlineEdit: { fields: ["name"] },
          linkToItem: true,
          inlineConnect: true,
          inlineCreate: { fields: ["name"] }
        }
      })
    }
  }),
  // this last list is our Tag list, it only has a name field for now
  Tag: (0, import_core.list)({
    access: import_access.allowAll,
    // setting this to isHidden for the user interface prevents this list being visible in the Admin UI
    ui: {
      isHidden: true
    },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true }, isIndexed: "unique" }),
      posts: (0, import_fields.relationship)({ ref: "Post.tags", many: true })
    }
  }),
  Reservation: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    // TODO add virtual field for reserveeName
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      }),
      reservationUnit: (0, import_fields.relationship)({
        ref: "ReservationUnit.reservations",
        many: false
      }),
      reserveeType: (0, import_fields.select)({
        options: [
          { label: "Person", value: "person" },
          { label: "Organization", value: "organization" },
          { label: "Business", value: "business" },
          { label: "Uknown", value: "unknown" }
        ],
        defaultValue: "unknown"
      }),
      reserveeName: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeFirstName: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeLastName: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeOrganizationName: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeEmail: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveePhone: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeAddressStreet: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeAddressCity: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeAddressZip: (0, import_fields.text)({ validation: { isRequired: false } }),
      reserveeIsUnregisteredAssociation: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      billingFirstName: (0, import_fields.text)({ validation: { isRequired: false } }),
      billingLastName: (0, import_fields.text)({ validation: { isRequired: false } }),
      billingPhone: (0, import_fields.text)({ validation: { isRequired: false } }),
      billingEmail: (0, import_fields.text)({ validation: { isRequired: false } }),
      billingAddressStreet: (0, import_fields.text)({ validation: { isRequired: false } }),
      billingAddressCity: (0, import_fields.text)({ validation: { isRequired: false } }),
      billingAddressZip: (0, import_fields.text)({ validation: { isRequired: false } }),
      homeCity: (0, import_fields.text)({ validation: { isRequired: false } }),
      // TODO this needs to be editable so use a relationship
      ageGroup: (0, import_fields.relationship)({ ref: "AgeGroup", many: false }),
      applyingForFreeOfCharge: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      description: (0, import_fields.text)({ validation: { isRequired: false } }),
      state: (0, import_fields.select)({
        options: [
          { label: "Created", value: "CREATED" },
          { label: "Cancelled", value: "CANCELLED" },
          { label: "Requires handling", value: "REQUIRES_HANDLING" },
          { label: "Waiting for payment", value: "WAITING_FOR_PAYMENT" },
          { label: "Confirmed", value: "CONFIRMED" },
          { label: "Denied", value: "DENIED" }
        ],
        defaultValue: "CREATED"
      }),
      priority: (0, import_fields.select)({
        options: [
          { label: "Low", value: "A_100" },
          { label: "Medium", value: "A_200" },
          { label: "High", value: "A_300" }
        ],
        defaultValue: "A_200"
      }),
      begin: (0, import_fields.timestamp)({ validation: { isRequired: true } }),
      end: (0, import_fields.timestamp)({ validation: { isRequired: true } }),
      bufferTimeBefore: (0, import_fields.integer)({ validation: { isRequired: false } }),
      bufferTimeAfter: (0, import_fields.integer)({ validation: { isRequired: false } }),
      recurringReservation: (0, import_fields.relationship)({
        ref: "RecurringReservation",
        many: false
      }),
      numPersons: (0, import_fields.integer)({ validation: { isRequired: false } }),
      purpose: (0, import_fields.relationship)({ ref: "ReservationPurpose", many: false }),
      serviceSector: (0, import_fields.relationship)({ ref: "ServiceSector", many: false }),
      cancelDetails: (0, import_fields.text)({ validation: { isRequired: false } }),
      unitPrice: (0, import_fields.float)({ validation: { isRequired: false, min: 0 } }),
      taxPercentageValue: (0, import_fields.float)({
        validation: { isRequired: false, min: 0 }
      }),
      price: (0, import_fields.float)({ validation: { isRequired: false, min: 0 } }),
      priceNet: (0, import_fields.float)({ validation: { isRequired: false, min: 0 } }),
      handledAt: (0, import_fields.timestamp)({ validation: { isRequired: false } }),
      workingMemory: (0, import_fields.text)({ validation: { isRequired: false } }),
      type: (0, import_fields.select)({
        options: [
          { label: "Blocked", value: "BLOCKED" },
          { label: "Behalf", value: "BEHALF" },
          { label: "Staff", value: "STAFF" },
          { label: "Normal", value: "NORMAL" }
        ],
        defaultValue: "NORMAL"
      }),
      orderUuid: (0, import_fields.text)({ validation: { isRequired: false } }),
      orderStatus: (0, import_fields.text)({ validation: { isRequired: false } }),
      refundUuid: (0, import_fields.text)({ validation: { isRequired: false } }),
      isHandled: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      calendarUrl: (0, import_fields.text)({ validation: { isRequired: false } })
    }
  }),
  ServiceSector: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  ReservationPurpose: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      description: (0, import_fields.text)({ validation: { isRequired: false } }),
      reservations: (0, import_fields.relationship)({ ref: "Reservation", many: true }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  RecurringReservation: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      description: (0, import_fields.text)({ validation: { isRequired: false } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      }),
      beginTime: (0, import_fields.text)({
        validation: { isRequired: true, match: { regex: /^[0-2]?\d:[0-5]\d/ } }
      }),
      endTime: (0, import_fields.text)({
        validation: { isRequired: true, match: { regex: /^[0-2]?\d:[0-5]\d/ } }
      }),
      beginDate: (0, import_fields.calendarDay)({
        validation: { isRequired: true }
      }),
      endDate: (0, import_fields.calendarDay)({
        validation: { isRequired: true }
      }),
      reservationUnit: (0, import_fields.relationship)({
        ref: "ReservationUnit.recurringReservations",
        many: false
      }),
      reservation: (0, import_fields.relationship)({ ref: "Reservation", many: true }),
      recuranceInDays: (0, import_fields.select)({
        options: [
          { label: "Every week", value: 7 },
          { label: "Every two weeks", value: 14 }
        ],
        type: "integer",
        defaultValue: 7
      }),
      weekdays: (0, import_fields.relationship)({
        ref: "Weekday.recurringReservations",
        many: true
      }),
      // TODO relationship
      ageGroup: (0, import_fields.relationship)({ ref: "AgeGroup", many: true })
    }
  }),
  Weekday: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      value: (0, import_fields.integer)({ validation: { isRequired: true, min: 0, max: 6 } }),
      recurringReservations: (0, import_fields.relationship)({
        ref: "RecurringReservation.weekdays",
        many: true
      }),
      openingHours: (0, import_fields.relationship)({
        ref: "OpeningHour.weekdays",
        many: true
      })
    }
  }),
  ReservationUnit: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      description: (0, import_fields.text)({ validation: { isRequired: false } }),
      reservations: (0, import_fields.relationship)({
        ref: "Reservation.reservationUnit",
        many: true
      }),
      recurringReservations: (0, import_fields.relationship)({
        ref: "RecurringReservation.reservationUnit",
        many: true
      }),
      isDraft: (0, import_fields.checkbox)({
        defaultValue: true
      }),
      contactInformation: (0, import_fields.text)({ validation: { isRequired: true } }),
      requiresIntroductions: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      reservationCancelledInstructions: (0, import_fields.text)({
        validation: { isRequired: false }
      }),
      reservationUnitType: (0, import_fields.select)({
        options: [
          { label: "Cottage", value: "cottage" },
          { label: "Sauna", value: "sauna" },
          { label: "Meeting room", value: "meeting-room" },
          { label: "Other", value: "other" }
        ],
        defaultValue: "cottage"
      }),
      // TODO TOCs (a custom type)
      // TODO number type
      maxPersons: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      minPersons: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      surfaceArea: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      // TODO duration type
      bufferTimeBefore: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      bufferTimeAfter: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      maxReservationDuration: (0, import_fields.integer)({
        validation: { isRequired: false, min: 0 }
      }),
      minReservationDuration: (0, import_fields.integer)({
        validation: { isRequired: false, min: 0 }
      }),
      cancellationRule: (0, import_fields.select)({
        options: [
          { label: "Free cancellation", value: "free" },
          { label: "No cancellation", value: "no" }
        ]
      }),
      reservationStartInterval: (0, import_fields.select)({
        options: [
          { label: "15 minutes", value: "15" },
          { label: "30 minutes", value: "30" },
          { label: "1 hour", value: "60" },
          { label: "90 minutes", value: "90" }
        ],
        defaultValue: "15"
      }),
      reservationsMaxDaysBefore: (0, import_fields.integer)({
        validation: { isRequired: false, min: 0 }
      }),
      reservationsMinDaysBefore: (0, import_fields.integer)({
        validation: { isRequired: false, min: 0 }
      }),
      // Times when available
      reservationBegins: (0, import_fields.timestamp)({ validation: { isRequired: false } }),
      reservationEnds: (0, import_fields.timestamp)({ validation: { isRequired: false } }),
      publishBegins: (0, import_fields.timestamp)({ validation: { isRequired: false } }),
      publishEnds: (0, import_fields.timestamp)({ validation: { isRequired: false } }),
      metadataSet: (0, import_fields.relationship)({ ref: "MetadataSet", many: false }),
      maxReservationsPerUser: (0, import_fields.integer)({
        validation: { isRequired: false, min: 0 }
      }),
      requireReservationHandling: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      authentication: (0, import_fields.select)({
        options: [
          { label: "Weak", value: "weak" },
          { label: "Strong", value: "strong" }
        ]
      }),
      rank: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      paymentTypes: (0, import_fields.relationship)({ ref: "PaymentType", many: true }),
      canApplyFreeForCharge: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      allowReservationsWithoutOpeningHours: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      isArchived: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      paymentMerchant: (0, import_fields.relationship)({ ref: "PaymentMerchant", many: false }),
      paymentProduct: (0, import_fields.relationship)({ ref: "PaymentProduct", many: false }),
      pricings: (0, import_fields.select)({
        options: [
          { label: "Free", value: "free" },
          { label: "Fixed", value: "fixed" },
          { label: "Variable", value: "variable" }
        ]
      }),
      // TODO images
      // TODO applicationRounds
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      }),
      location: (0, import_fields.select)({
        options: [
          { label: "Helsinki", value: "helsinki" },
          { label: "Muu", value: "muu" }
        ]
      }),
      // equipments: relationship({ ref: 'Equipment', many: true }),
      unit: (0, import_fields.relationship)({ ref: "Unit", many: false }),
      state: (0, import_fields.select)({
        options: [
          { label: "Draft", value: "DRAFT" },
          { label: "Published", value: "PUBLISHED" },
          { label: "Archived", value: "ARCHIVED" },
          { label: "Hidden", value: "HIDDEN" },
          { label: "Scheduled publishing", value: "SCHEDULED_PUBLISHING" },
          { label: "Scheduled hiding", value: "SCHEDULED_HIDING" },
          { label: "Scheduled period", value: "SCHEDULED_PERIOD" }
        ]
      }),
      reservationState: (0, import_fields.select)({
        options: [
          { label: "Created", value: "CREATED" },
          { label: "Cancelled", value: "CANCELLED" },
          { label: "Requires handling", value: "REQUIRES_HANDLING" },
          { label: "Waiting for payment", value: "WAITING_FOR_PAYMENT" },
          { label: "Confirmed", value: "CONFIRMED" },
          { label: "Denied", value: "DENIED" }
        ]
      })
      // TODO Keyword groups
    }
  }),
  Unit: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      description: (0, import_fields.text)({ validation: { isRequired: false } }),
      shortDescription: (0, import_fields.text)({ validation: { isRequired: false } }),
      // TODO regex validation
      webPage: (0, import_fields.text)({ validation: { isRequired: false } }),
      email: (0, import_fields.text)({ validation: { isRequired: false } }),
      phone: (0, import_fields.text)({ validation: { isRequired: false } }),
      openingHours: (0, import_fields.relationship)({ ref: "OpeningHour.units", many: true }),
      paymentMerchange: (0, import_fields.relationship)({ ref: "PaymentMerchant", many: false }),
      reservationUnits: (0, import_fields.relationship)({ ref: "ReservationUnit", many: true }),
      spaces: (0, import_fields.relationship)({ ref: "Space", many: true })
      // TODO location
      // TODO serviceSectors
    }
  }),
  OpeningHour: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      // TODO times should probably be HH:MM format (or integers with a custom input component)
      from: (0, import_fields.calendarDay)({ validation: { isRequired: true } }),
      to: (0, import_fields.calendarDay)({ validation: { isRequired: true } }),
      weekdays: (0, import_fields.relationship)({ ref: "Weekday.openingHours", many: true }),
      opens: (0, import_fields.integer)({ validation: { isRequired: false } }),
      closes: (0, import_fields.integer)({ validation: { isRequired: false } }),
      state: (0, import_fields.text)({ validation: { isRequired: false } }),
      isReservable: (0, import_fields.checkbox)({
        defaultValue: false
      }),
      units: (0, import_fields.relationship)({ ref: "Unit.openingHours", many: true })
      // TODO
    }
  }),
  Space: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      surfaceArea: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      maxPersons: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      code: (0, import_fields.text)({ validation: { isRequired: false } }),
      unit: (0, import_fields.relationship)({ ref: "Unit", many: false }),
      parent: (0, import_fields.relationship)({ ref: "Space", many: false }),
      children: (0, import_fields.relationship)({ ref: "Space", many: true }),
      resources: (0, import_fields.relationship)({ ref: "Resource", many: true }),
      // TODO building missing
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  Resource: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      description: (0, import_fields.text)({ validation: { isRequired: false } }),
      space: (0, import_fields.relationship)({ ref: "Space", many: false })
    }
  }),
  TermsOfUseTranslation: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      text: (0, import_fields.text)({ validation: { isRequired: true } }),
      language: (0, import_fields.select)({
        options: [
          { label: "Finnish", value: "fi" },
          { label: "Swedish", value: "sv" },
          { label: "English", value: "en" }
        ]
      }),
      termsOfUse: (0, import_fields.relationship)({ ref: "TermsOfUse", many: false }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  TermsOfUse: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      text: (0, import_fields.text)({ validation: { isRequired: true } }),
      translations: (0, import_fields.relationship)({ ref: "TermsOfUseTranslation", many: true }),
      termsType: (0, import_fields.select)({
        options: [
          { label: "Generic terms", value: "GENERIC_TERMS" },
          { label: "Payment terms", value: "PAYMENT_TERMS" },
          { label: "Cancellation terms", value: "CANCELLATION_TERMS" },
          { label: "Recurring reservation terms", value: "RECURRING_TERMS" },
          { label: "Service-specific terms", value: "SERVICE_TERMS" },
          { label: "Pricing terms", value: "PRICING_TERMS" }
        ]
      }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  EquipmentCategory: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      }),
      equipments: (0, import_fields.relationship)({ ref: "Equipment", many: true })
    }
  }),
  Equipment: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    graphql: {
      description: "...",
      plural: "equipments"
    },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      category: (0, import_fields.relationship)({ ref: "EquipmentCategory", many: false }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  AgeGroup: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      min: (0, import_fields.integer)({ validation: { isRequired: true, min: 0 } }),
      max: (0, import_fields.integer)({ validation: { isRequired: false, min: 0 } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      }),
      recurringReservations: (0, import_fields.relationship)({
        ref: "RecurringReservation",
        many: true
      }),
      reservations: (0, import_fields.relationship)({ ref: "Reservation", many: true })
    }
  }),
  AbilityGroup: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  ReservationCancelReason: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      reason: (0, import_fields.text)({ validation: { isRequired: true } }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
    }
  }),
  // TODO is this necessary type? can it be replaced with an enum?
  PaymentTypeOption: (0, import_core.list)({
    access: import_access.allowAll,
    fields: {
      code: (0, import_fields.select)({
        options: [
          { label: "Online", value: "ONLINE" },
          { label: "Invoice", value: "INVOICE" },
          { label: "On site", value: "ON_SITE" }
        ],
        defaultValue: "ONLINE"
      })
    }
  }),
  PaymentProduct: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } })
      // TODO
      // reservationUnits: relationship({ ref: 'PaymentProduct.reservationUnits', many: true }),
    }
  }),
  PaymentMerchant: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } })
      // reservationUnits: relationship({ ref: 'PaymentMerchant.reservationUnits', many: true }),
      // TODO
    }
  }),
  PaymentType: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } })
      // reservationUnits: relationship({ ref: 'PaymentType.reservationUnits', many: true }),
      // TODO
    }
  }),
  MetadataSet: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      supportedFields: (0, import_fields.relationship)({ ref: "MetadataField", many: true }),
      requiredFields: (0, import_fields.relationship)({ ref: "MetadataField", many: true }),
      pk: (0, import_fields.integer)({
        validation: { isRequired: true, min: 1 },
        isIndexed: "unique"
      })
      // reservationUnits: relationship({ ref: 'ReservationUnit.metadataSet', many: true }),
    }
  }),
  MetadataField: (0, import_core.list)({
    access: import_access.allowAll,
    // ui: { },
    fields: {
      name: (0, import_fields.text)({ validation: { isRequired: true } }),
      value: (0, import_fields.text)({ validation: { isRequired: true } }),
      type: (0, import_fields.select)({
        options: [
          { label: "Text", value: "TEXT" },
          { label: "Number", value: "NUMBER" },
          { label: "Date", value: "DATE" },
          { label: "Single select", value: "SINGLE_SELECT" },
          { label: "Multi select", value: "MULTI_SELECT" },
          { label: "Checkbox", value: "CHECKBOX" }
        ],
        defaultValue: "TEXT"
      })
    }
  })
};

// auth.ts
var import_crypto = require("crypto");
var import_auth = require("@keystone-6/auth");
var import_session = require("@keystone-6/core/session");
var sessionSecret = process.env.SESSION_SECRET;
if (!sessionSecret && process.env.NODE_ENV !== "production") {
  sessionSecret = (0, import_crypto.randomBytes)(32).toString("hex");
}
var { withAuth } = (0, import_auth.createAuth)({
  listKey: "User",
  identityField: "email",
  // this is a GraphQL query fragment for fetching what data will be attached to a context.session
  //   this can be helpful for when you are writing your access control functions
  //   you can find out more at https://keystonejs.com/docs/guides/auth-and-access-control
  sessionData: "name createdAt",
  secretField: "password",
  // WARNING: remove initFirstItem functionality in production
  //   see https://keystonejs.com/docs/config/auth#init-first-item for more
  initFirstItem: {
    // if there are no items in the database, by configuring this field
    //   you are asking the Keystone AdminUI to create a new user
    //   providing inputs for these fields
    fields: ["name", "email", "password"]
    // it uses context.sudo() to do this, which bypasses any access control you might have
    //   you shouldn't use this in production
  }
});
var sessionMaxAge = 60 * 60 * 24 * 30;
var session = (0, import_session.statelessSessions)({
  maxAge: sessionMaxAge,
  secret: sessionSecret
});

// seed.ts
async function seed(prisma) {
  console.log("Seeding database");
  const tag = await prisma.tag.upsert({
    where: { name: "Tag" },
    create: {
      name: "Tag"
    },
    update: {}
  });
  const purposes = await Promise.all(
    ["Purpose", "Working"].map(
      async (name, i) => prisma.reservationPurpose.upsert({
        where: { pk: i + 1 },
        create: {
          pk: i + 1,
          name,
          description: `Description for ${name}`
        },
        update: {}
      })
    )
  );
  Array.from(Array(10).keys()).forEach(async (i) => {
    await prisma.post.create({
      data: {
        title: `Post ${i}`,
        // TODO this doesn't work because it's a Document field not a string
        // content: `Content ${i}`.concat(lorem),
        tags: {
          connect: {
            id: tag.id
          }
        }
      }
    });
    const now = /* @__PURE__ */ new Date();
    const ru2 = await prisma.reservationUnit.upsert({
      where: { pk: i + 1 },
      update: {},
      create: {
        pk: i + 1,
        name: `Reservation Unit ${i + 1}`,
        description: `Description ${i + 1}`,
        minPersons: i * 5,
        maxPersons: i * 10,
        surfaceArea: i * 100,
        bufferTimeAfter: 15,
        bufferTimeBefore: 15,
        maxReservationDuration: 120,
        minReservationDuration: 30,
        reservationBegins: now,
        reservationEnds: new Date(
          now.getFullYear() + 1,
          now.getMonth(),
          now.getDate()
        ),
        publishBegins: now,
        publishEnds: new Date(
          now.getFullYear() + 1,
          now.getMonth(),
          now.getDate()
        ),
        reservations: {
          create: Array.from(Array(10).keys()).map((j) => ({
            pk: i * 10 + j + 1,
            name: `Reservation ${i + 1}-${j + 1}`,
            purpose: {
              connect: {
                pk: purposes[j % purposes.length].pk
              }
            },
            begin: new Date(
              now.getFullYear(),
              now.getMonth(),
              now.getDate() + j,
              now.getHours()
            ),
            end: new Date(
              now.getFullYear(),
              now.getMonth(),
              now.getDate() + j,
              now.getHours() + 2
            )
          }))
        }
      }
    });
  });
  const ru = await prisma.reservationUnit.findFirst({ where: { pk: 1 } });
  const recurringStart = /* @__PURE__ */ new Date();
  const recurringEnd = new Date((/* @__PURE__ */ new Date()).getFullYear() + 1, 11, 31);
  const interval = recurringEnd.getTime() - recurringStart.getTime();
  const reservationsForRecurring = Array.from(
    Array(Math.ceil(interval / (24 * 60 * 60 * 1e3))).keys()
  ).map((i) => ({
    pk: i + 1 + 1e3,
    name: `Reservation ${i + 1}`,
    purpose: {
      connect: {
        id: purposes[i % purposes.length].id
      }
    },
    begin: new Date(
      recurringStart.getFullYear(),
      recurringStart.getMonth(),
      recurringStart.getDate() + i,
      recurringStart.getHours()
    ),
    end: new Date(
      recurringStart.getFullYear(),
      recurringStart.getMonth(),
      recurringStart.getDate() + i,
      recurringStart.getHours() + 2
    )
  }));
  await prisma.recurringReservation.upsert({
    where: { pk: 1 },
    update: {
      name: "Recurring Reservation",
      description: "Test recurring reservation",
      beginTime: "08:00",
      endTime: "10:00",
      beginDate: ((d) => `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(
        recurringStart
      ),
      endDate: ((d) => `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(recurringEnd)
    },
    create: {
      name: "Recurring Reservation",
      description: "Test recurring reservation",
      pk: 1,
      beginTime: "08:00",
      endTime: "10:00",
      beginDate: ((d) => `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(
        recurringStart
      ),
      endDate: ((d) => `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(recurringEnd),
      reservationUnit: {
        connect: {
          id: ru?.id
        }
      },
      reservation: {
        create: reservationsForRecurring
      },
      recuranceInDays: 7,
      weekdays: {
        create: [
          { name: "Monday", value: 1 },
          { name: "Tuesday", value: 2 },
          { name: "Wednesday", value: 3 },
          { name: "Thursday", value: 4 },
          { name: "Friday", value: 5 },
          { name: "Saturday", value: 6 },
          { name: "Sunday", value: 0 }
        ].map((day) => ({ name: day.name, value: day.value }))
      }
    }
  });
}

// keystone.ts
var keystone_default = withAuth(
  (0, import_core2.config)({
    server: {
      cors: { origin: ["http://localhost:3001"], credentials: true }
    },
    db: {
      // we're using sqlite for the fastest startup experience
      //   for more information on what database might be appropriate for you
      //   see https://keystonejs.com/docs/guides/choosing-a-database#title
      provider: "sqlite",
      url: "file:./keystone.db?connection_limit=1",
      onConnect: async (context) => {
        seed(context.prisma);
      }
    },
    lists,
    session
  })
);
