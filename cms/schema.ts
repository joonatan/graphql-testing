// Welcome to your schema
//   Schema driven development is Keystone's modus operandi
//
// This file is where we define the lists, fields and hooks for our data.
// If you want to learn more about how lists are configured, please read
// - https://keystonejs.com/docs/config/lists

import { list } from '@keystone-6/core'
import { allowAll } from '@keystone-6/core/access'

// see https://keystonejs.com/docs/fields/overview for the full list of fields
//   this is a few common fields for an example
import {
  text,
  relationship,
  password,
  timestamp,
  select,
  checkbox,
  integer,
  float,
  calendarDay,
} from '@keystone-6/core/fields'

// the document field is a more complicated field, so it has it's own package
import { document } from '@keystone-6/fields-document'
// if you want to make your own fields, see https://keystonejs.com/docs/guides/custom-fields

// when using Typescript, you can refine your types to a stricter subset by importing
// the generated types from '.keystone/types'
import type { Lists } from '.keystone/types'

export const lists: Lists = {
  User: list({
    // WARNING
    //   for this starter project, anyone can create, query, update and delete anything
    //   if you want to prevent random people on the internet from accessing your data,
    //   you can find out more at https://keystonejs.com/docs/guides/auth-and-access-control
    access: allowAll,

    // this is the fields for our User list
    fields: {
      // by adding isRequired, we enforce that every User should have a name
      //   if no name is provided, an error will be displayed
      name: text({ validation: { isRequired: true } }),

      email: text({
        validation: { isRequired: true },
        // by adding isIndexed: 'unique', we're saying that no user can have the same
        // email as another user - this may or may not be a good idea for your project
        isIndexed: 'unique',
      }),

      password: password({ validation: { isRequired: true } }),

      // we can use this field to see what Posts this User has authored
      //   more on that in the Post list below
      posts: relationship({ ref: 'Post.author', many: true }),

      createdAt: timestamp({
        // this sets the timestamp to Date.now() when the user is first created
        defaultValue: { kind: 'now' },
      }),
    },
  }),

  Post: list({
    // WARNING
    //   for this starter project, anyone can create, query, update and delete anything
    //   if you want to prevent random people on the internet from accessing your data,
    //   you can find out more at https://keystonejs.com/docs/guides/auth-and-access-control
    access: allowAll,

    // this is the fields for our Post list
    fields: {
      title: text({ validation: { isRequired: true } }),

      // the document field can be used for making rich editable content
      //   you can find out more at https://keystonejs.com/docs/guides/document-fields
      content: document({
        formatting: true,
        layouts: [
          [1, 1],
          [1, 1, 1],
          [2, 1],
          [1, 2],
          [1, 2, 1],
        ],
        links: true,
        dividers: true,
      }),

      // with this field, you can set a User as the author for a Post
      author: relationship({
        // we could have used 'User', but then the relationship would only be 1-way
        ref: 'User.posts',

        // this is some customisations for changing how this will look in the AdminUI
        ui: {
          displayMode: 'cards',
          cardFields: ['name', 'email'],
          inlineEdit: { fields: ['name', 'email'] },
          linkToItem: true,
          inlineConnect: true,
        },

        // a Post can only have one author
        //   this is the default, but we show it here for verbosity
        many: false,
      }),

      // with this field, you can add some Tags to Posts
      tags: relationship({
        // we could have used 'Tag', but then the relationship would only be 1-way
        ref: 'Tag.posts',

        // a Post can have many Tags, not just one
        many: true,

        // this is some customisations for changing how this will look in the AdminUI
        ui: {
          displayMode: 'cards',
          cardFields: ['name'],
          inlineEdit: { fields: ['name'] },
          linkToItem: true,
          inlineConnect: true,
          inlineCreate: { fields: ['name'] },
        },
      }),
    },
  }),

  // this last list is our Tag list, it only has a name field for now
  Tag: list({
    access: allowAll,
    // setting this to isHidden for the user interface prevents this list being visible in the Admin UI
    ui: {
      isHidden: true,
    },
    fields: {
      name: text({ validation: { isRequired: true }, isIndexed: 'unique' }),
      posts: relationship({ ref: 'Post.tags', many: true }),
    },
  }),

  Reservation: list({
    access: allowAll,
    // ui: { },
    // TODO add virtual field for reserveeName
    fields: {
      name: text({ validation: { isRequired: true } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
      reservationUnit: relationship({
        ref: 'ReservationUnit.reservations',
        many: false,
      }),
      reserveeType: select({
        options: [
          { label: 'Person', value: 'person' },
          { label: 'Organization', value: 'organization' },
          { label: 'Business', value: 'business' },
          { label: 'Uknown', value: 'unknown' },
        ],
        defaultValue: 'unknown',
      }),
      reserveeName: text({ validation: { isRequired: false } }),
      reserveeFirstName: text({ validation: { isRequired: false } }),
      reserveeLastName: text({ validation: { isRequired: false } }),
      reserveeOrganizationName: text({ validation: { isRequired: false } }),
      reserveeEmail: text({ validation: { isRequired: false } }),
      reserveePhone: text({ validation: { isRequired: false } }),
      reserveeAddressStreet: text({ validation: { isRequired: false } }),
      reserveeAddressCity: text({ validation: { isRequired: false } }),
      reserveeAddressZip: text({ validation: { isRequired: false } }),
      reserveeIsUnregisteredAssociation: checkbox({
        defaultValue: false,
      }),
      billingFirstName: text({ validation: { isRequired: false } }),
      billingLastName: text({ validation: { isRequired: false } }),
      billingPhone: text({ validation: { isRequired: false } }),
      billingEmail: text({ validation: { isRequired: false } }),
      billingAddressStreet: text({ validation: { isRequired: false } }),
      billingAddressCity: text({ validation: { isRequired: false } }),
      billingAddressZip: text({ validation: { isRequired: false } }),

      homeCity: text({ validation: { isRequired: false } }),
      // TODO this needs to be editable so use a relationship
      ageGroup: relationship({ ref: 'AgeGroup', many: false }),
      applyingForFreeOfCharge: checkbox({
        defaultValue: false,
      }),
      description: text({ validation: { isRequired: false } }),
      state: select({
        options: [
          { label: 'Created', value: 'CREATED' },
          { label: 'Cancelled', value: 'CANCELLED' },
          { label: 'Requires handling', value: 'REQUIRES_HANDLING' },
          { label: 'Waiting for payment', value: 'WAITING_FOR_PAYMENT' },
          { label: 'Confirmed', value: 'CONFIRMED' },
          { label: 'Denied', value: 'DENIED' },
        ],
        defaultValue: 'CREATED',
      }),
      priority: select({
        options: [
          { label: 'Low', value: 'A_100' },
          { label: 'Medium', value: 'A_200' },
          { label: 'High', value: 'A_300' },
        ],
        defaultValue: 'A_200',
      }),
      begin: timestamp({ validation: { isRequired: true } }),
      end: timestamp({ validation: { isRequired: true } }),
      bufferTimeBefore: integer({ validation: { isRequired: false } }),
      bufferTimeAfter: integer({ validation: { isRequired: false } }),
      recurringReservation: relationship({
        ref: 'RecurringReservation',
        many: false,
      }),
      numPersons: integer({ validation: { isRequired: false } }),
      purpose: relationship({ ref: 'ReservationPurpose', many: false }),
      serviceSector: relationship({ ref: 'ServiceSector', many: false }),
      cancelDetails: text({ validation: { isRequired: false } }),
      unitPrice: float({ validation: { isRequired: false, min: 0 } }),
      taxPercentageValue: float({
        validation: { isRequired: false, min: 0 },
      }),
      price: float({ validation: { isRequired: false, min: 0 } }),
      priceNet: float({ validation: { isRequired: false, min: 0 } }),
      handledAt: timestamp({ validation: { isRequired: false } }),
      workingMemory: text({ validation: { isRequired: false } }),
      type: select({
        options: [
          { label: 'Blocked', value: 'BLOCKED' },
          { label: 'Behalf', value: 'BEHALF' },
          { label: 'Staff', value: 'STAFF' },
          { label: 'Normal', value: 'NORMAL' },
        ],
        defaultValue: 'NORMAL',
      }),
      orderUuid: text({ validation: { isRequired: false } }),
      orderStatus: text({ validation: { isRequired: false } }),
      refundUuid: text({ validation: { isRequired: false } }),
      isHandled: checkbox({
        defaultValue: false,
      }),
      calendarUrl: text({ validation: { isRequired: false } }),
    },
  }),

  ServiceSector: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  ReservationPurpose: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      description: text({ validation: { isRequired: false } }),
      reservations: relationship({ ref: 'Reservation', many: true }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  RecurringReservation: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      description: text({ validation: { isRequired: false } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
      beginTime: text({
        validation: { isRequired: true, match: { regex: /^[0-2]?\d:[0-5]\d/ } },
      }),
      endTime: text({
        validation: { isRequired: true, match: { regex: /^[0-2]?\d:[0-5]\d/ } },
      }),
      beginDate: calendarDay({
        validation: { isRequired: true },
      }),
      endDate: calendarDay({
        validation: { isRequired: true },
      }),
      reservationUnit: relationship({
        ref: 'ReservationUnit.recurringReservations',
        many: false,
      }),
      reservation: relationship({ ref: 'Reservation', many: true }),
      recuranceInDays: select({
        options: [
          { label: 'Every week', value: 7 },
          { label: 'Every two weeks', value: 14 },
        ],
        type: 'integer',
        defaultValue: 7,
      }),
      weekdays: relationship({
        ref: 'Weekday.recurringReservations',
        many: true,
      }),
      // TODO relationship
      ageGroup: relationship({ ref: 'AgeGroup', many: true }),
    },
  }),

  Weekday: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      value: integer({ validation: { isRequired: true, min: 0, max: 6 } }),
      recurringReservations: relationship({
        ref: 'RecurringReservation.weekdays',
        many: true,
      }),
      openingHours: relationship({
        ref: 'OpeningHour.weekdays',
        many: true,
      }),
    },
  }),

  ReservationUnit: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      description: text({ validation: { isRequired: false } }),
      reservations: relationship({
        ref: 'Reservation.reservationUnit',
        many: true,
      }),
      recurringReservations: relationship({
        ref: 'RecurringReservation.reservationUnit',
        many: true,
      }),
      isDraft: checkbox({
        defaultValue: true,
      }),
      contactInformation: text({ validation: { isRequired: true } }),
      requiresIntroductions: checkbox({
        defaultValue: false,
      }),
      reservationCancelledInstructions: text({
        validation: { isRequired: false },
      }),
      reservationUnitType: select({
        options: [
          { label: 'Cottage', value: 'cottage' },
          { label: 'Sauna', value: 'sauna' },
          { label: 'Meeting room', value: 'meeting-room' },
          { label: 'Other', value: 'other' },
        ],
        defaultValue: 'cottage',
      }),
      // TODO TOCs (a custom type)
      // TODO number type
      maxPersons: integer({ validation: { isRequired: false, min: 0 } }),
      minPersons: integer({ validation: { isRequired: false, min: 0 } }),
      surfaceArea: integer({ validation: { isRequired: false, min: 0 } }),
      // TODO duration type
      bufferTimeBefore: integer({ validation: { isRequired: false, min: 0 } }),
      bufferTimeAfter: integer({ validation: { isRequired: false, min: 0 } }),
      maxReservationDuration: integer({
        validation: { isRequired: false, min: 0 },
      }),
      minReservationDuration: integer({
        validation: { isRequired: false, min: 0 },
      }),
      cancellationRule: select({
        options: [
          { label: 'Free cancellation', value: 'free' },
          { label: 'No cancellation', value: 'no' },
        ],
      }),
      reservationStartInterval: select({
        options: [
          { label: '15 minutes', value: '15' },
          { label: '30 minutes', value: '30' },
          { label: '1 hour', value: '60' },
          { label: '90 minutes', value: '90' },
        ],
        defaultValue: '15',
      }),
      reservationsMaxDaysBefore: integer({
        validation: { isRequired: false, min: 0 },
      }),
      reservationsMinDaysBefore: integer({
        validation: { isRequired: false, min: 0 },
      }),
      // Times when available
      reservationBegins: timestamp({ validation: { isRequired: false } }),
      reservationEnds: timestamp({ validation: { isRequired: false } }),
      publishBegins: timestamp({ validation: { isRequired: false } }),
      publishEnds: timestamp({ validation: { isRequired: false } }),

      metadataSet: relationship({ ref: 'MetadataSet', many: false }),
      maxReservationsPerUser: integer({
        validation: { isRequired: false, min: 0 },
      }),
      requireReservationHandling: checkbox({
        defaultValue: false,
      }),
      authentication: select({
        options: [
          { label: 'Weak', value: 'weak' },
          { label: 'Strong', value: 'strong' },
        ],
      }),
      rank: integer({ validation: { isRequired: false, min: 0 } }),
      paymentTypes: relationship({ ref: 'PaymentType', many: true }),
      canApplyFreeForCharge: checkbox({
        defaultValue: false,
      }),
      allowReservationsWithoutOpeningHours: checkbox({
        defaultValue: false,
      }),
      isArchived: checkbox({
        defaultValue: false,
      }),
      paymentMerchant: relationship({ ref: 'PaymentMerchant', many: false }),
      paymentProduct: relationship({ ref: 'PaymentProduct', many: false }),
      pricings: select({
        options: [
          { label: 'Free', value: 'free' },
          { label: 'Fixed', value: 'fixed' },
          { label: 'Variable', value: 'variable' },
        ],
      }),
      // TODO images
      // TODO applicationRounds
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
      location: select({
        options: [
          { label: 'Helsinki', value: 'helsinki' },
          { label: 'Muu', value: 'muu' },
        ],
      }),
      // equipments: relationship({ ref: 'Equipment', many: true }),
      unit: relationship({ ref: 'Unit', many: false }),
      state: select({
        options: [
          { label: 'Draft', value: 'DRAFT' },
          { label: 'Published', value: 'PUBLISHED' },
          { label: 'Archived', value: 'ARCHIVED' },
          { label: 'Hidden', value: 'HIDDEN' },
          { label: 'Scheduled publishing', value: 'SCHEDULED_PUBLISHING' },
          { label: 'Scheduled hiding', value: 'SCHEDULED_HIDING' },
          { label: 'Scheduled period', value: 'SCHEDULED_PERIOD' },
        ],
      }),
      reservationState: select({
        options: [
          { label: 'Created', value: 'CREATED' },
          { label: 'Cancelled', value: 'CANCELLED' },
          { label: 'Requires handling', value: 'REQUIRES_HANDLING' },
          { label: 'Waiting for payment', value: 'WAITING_FOR_PAYMENT' },
          { label: 'Confirmed', value: 'CONFIRMED' },
          { label: 'Denied', value: 'DENIED' },
        ],
      }),
      // TODO Keyword groups
    },
  }),

  Unit: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      description: text({ validation: { isRequired: false } }),
      shortDescription: text({ validation: { isRequired: false } }),
      // TODO regex validation
      webPage: text({ validation: { isRequired: false } }),
      email: text({ validation: { isRequired: false } }),
      phone: text({ validation: { isRequired: false } }),
      openingHours: relationship({ ref: 'OpeningHour.units', many: true }),
      paymentMerchange: relationship({ ref: 'PaymentMerchant', many: false }),
      reservationUnits: relationship({ ref: 'ReservationUnit', many: true }),
      spaces: relationship({ ref: 'Space', many: true }),
      // TODO location
      // TODO serviceSectors
    },
  }),

  OpeningHour: list({
    access: allowAll,
    // ui: { },
    fields: {
      // TODO times should probably be HH:MM format (or integers with a custom input component)
      from: calendarDay({ validation: { isRequired: true } }),
      to: calendarDay({ validation: { isRequired: true } }),
      weekdays: relationship({ ref: 'Weekday.openingHours', many: true }),
      opens: integer({ validation: { isRequired: false } }),
      closes: integer({ validation: { isRequired: false } }),
      state: text({ validation: { isRequired: false } }),
      isReservable: checkbox({
        defaultValue: false,
      }),
      units: relationship({ ref: 'Unit.openingHours', many: true }),
      // TODO
    },
  }),

  Space: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      surfaceArea: integer({ validation: { isRequired: false, min: 0 } }),
      maxPersons: integer({ validation: { isRequired: false, min: 0 } }),
      code: text({ validation: { isRequired: false } }),
      unit: relationship({ ref: 'Unit', many: false }),
      parent: relationship({ ref: 'Space', many: false }),
      children: relationship({ ref: 'Space', many: true }),
      resources: relationship({ ref: 'Resource', many: true }),
      // TODO building missing
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  Resource: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      description: text({ validation: { isRequired: false } }),
      space: relationship({ ref: 'Space', many: false }),
    },
  }),

  TermsOfUseTranslation: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      text: text({ validation: { isRequired: true } }),
      language: select({
        options: [
          { label: 'Finnish', value: 'fi' },
          { label: 'Swedish', value: 'sv' },
          { label: 'English', value: 'en' },
        ],
      }),
      termsOfUse: relationship({ ref: 'TermsOfUse', many: false }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  TermsOfUse: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      text: text({ validation: { isRequired: true } }),
      translations: relationship({ ref: 'TermsOfUseTranslation', many: true }),
      termsType: select({
        options: [
          { label: 'Generic terms', value: 'GENERIC_TERMS' },
          { label: 'Payment terms', value: 'PAYMENT_TERMS' },
          { label: 'Cancellation terms', value: 'CANCELLATION_TERMS' },
          { label: 'Recurring reservation terms', value: 'RECURRING_TERMS' },
          { label: 'Service-specific terms', value: 'SERVICE_TERMS' },
          { label: 'Pricing terms', value: 'PRICING_TERMS' },
        ],
      }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  EquipmentCategory: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
      equipments: relationship({ ref: 'Equipment', many: true }),
    },
  }),

  Equipment: list({
    access: allowAll,
    // ui: { },
    graphql: {
      description: '...',
      plural: 'equipments',
    },
    fields: {
      name: text({ validation: { isRequired: true } }),
      category: relationship({ ref: 'EquipmentCategory', many: false }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  AgeGroup: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      min: integer({ validation: { isRequired: true, min: 0 } }),
      max: integer({ validation: { isRequired: false, min: 0 } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
      recurringReservations: relationship({
        ref: 'RecurringReservation',
        many: true,
      }),
      reservations: relationship({ ref: 'Reservation', many: true }),
    },
  }),

  AbilityGroup: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  ReservationCancelReason: list({
    access: allowAll,
    // ui: { },
    fields: {
      reason: text({ validation: { isRequired: true } }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
    },
  }),

  // TODO is this necessary type? can it be replaced with an enum?
  PaymentTypeOption: list({
    access: allowAll,
    fields: {
      code: select({
        options: [
          { label: 'Online', value: 'ONLINE' },
          { label: 'Invoice', value: 'INVOICE' },
          { label: 'On site', value: 'ON_SITE' },
        ],
        defaultValue: 'ONLINE',
      }),
    },
  }),

  PaymentProduct: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      // TODO
      // reservationUnits: relationship({ ref: 'PaymentProduct.reservationUnits', many: true }),
    },
  }),

  PaymentMerchant: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      // reservationUnits: relationship({ ref: 'PaymentMerchant.reservationUnits', many: true }),
      // TODO
    },
  }),

  PaymentType: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      // reservationUnits: relationship({ ref: 'PaymentType.reservationUnits', many: true }),
      // TODO
    },
  }),

  MetadataSet: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      supportedFields: relationship({ ref: 'MetadataField', many: true }),
      requiredFields: relationship({ ref: 'MetadataField', many: true }),
      pk: integer({
        validation: { isRequired: true, min: 1 },
        isIndexed: 'unique',
      }),
      // reservationUnits: relationship({ ref: 'ReservationUnit.metadataSet', many: true }),
    },
  }),

  MetadataField: list({
    access: allowAll,
    // ui: { },
    fields: {
      name: text({ validation: { isRequired: true } }),
      value: text({ validation: { isRequired: true } }),
      type: select({
        options: [
          { label: 'Text', value: 'TEXT' },
          { label: 'Number', value: 'NUMBER' },
          { label: 'Date', value: 'DATE' },
          { label: 'Single select', value: 'SINGLE_SELECT' },
          { label: 'Multi select', value: 'MULTI_SELECT' },
          { label: 'Checkbox', value: 'CHECKBOX' },
        ],
        defaultValue: 'TEXT',
      }),
    },
  }),
}
