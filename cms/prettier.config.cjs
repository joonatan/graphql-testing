/** @type {import("prettier").Config} */
const config = {
  singleQuote: true,
  semi: false,
}

module.exports = config
