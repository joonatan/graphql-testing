import { PrismaClient } from '@prisma/client'

export default async function seed(prisma: PrismaClient) {
  /* TODO this check doesn't work
  if (process.env.NODE_ENV !== 'development') {
    console.log('NOT Seeding database: not in development mode')
    return
  }
  */
  /*
  if ((await prisma.user.findFirst({ where: { email: 'admin' } })) != null) {
    console.log('NOT Seeding database: admin user already exists')
    return
  }
  */

  console.log('Seeding database')

  const tag = await prisma.tag.upsert({
    where: { name: 'Tag' },
    create: {
      name: 'Tag',
    },
    update: {},
  })

  const purposes = await Promise.all(
    ['Purpose', 'Working'].map(async (name, i) =>
      prisma.reservationPurpose.upsert({
        where: { pk: i + 1 },
        create: {
          pk: i + 1,
          name: name,
          description: `Description for ${name}`,
        },
        update: {},
      })
    )
  )

  Array.from(Array(10).keys()).forEach(async (i) => {
    await prisma.post.create({
      data: {
        title: `Post ${i}`,
        // TODO this doesn't work because it's a Document field not a string
        // content: `Content ${i}`.concat(lorem),
        tags: {
          connect: {
            id: tag.id,
          },
        },
      },
    })

    const now = new Date()
    const ru = await prisma.reservationUnit.upsert({
      where: { pk: i + 1 },
      update: {},
      create: {
        pk: i + 1,
        name: `Reservation Unit ${i + 1}`,
        description: `Description ${i + 1}`,
        minPersons: i * 5,
        maxPersons: i * 10,
        surfaceArea: i * 100,
        bufferTimeAfter: 15,
        bufferTimeBefore: 15,
        maxReservationDuration: 120,
        minReservationDuration: 30,
        reservationBegins: now,
        reservationEnds: new Date(
          now.getFullYear() + 1,
          now.getMonth(),
          now.getDate()
        ),
        publishBegins: now,
        publishEnds: new Date(
          now.getFullYear() + 1,
          now.getMonth(),
          now.getDate()
        ),
        reservations: {
          create: Array.from(Array(10).keys()).map((j) => ({
            pk: i * 10 + j + 1,
            name: `Reservation ${i + 1}-${j + 1}`,
            purpose: {
              connect: {
                pk: purposes[j % purposes.length].pk,
              },
            },
            begin: new Date(
              now.getFullYear(),
              now.getMonth(),
              now.getDate() + j,
              now.getHours()
            ),
            end: new Date(
              now.getFullYear(),
              now.getMonth(),
              now.getDate() + j,
              now.getHours() + 2
            ),
          })),
        },
      },
    })
  })

  // TODO check that we don't have any recurring reservations already
  const ru = await prisma.reservationUnit.findFirst({ where: { pk: 1 } })
  const recurringStart = new Date()
  const recurringEnd = new Date(new Date().getFullYear() + 1, 11, 31)
  const interval = recurringEnd.getTime() - recurringStart.getTime()
  const reservationsForRecurring = Array.from(
    Array(Math.ceil(interval / (24 * 60 * 60 * 1000))).keys()
  ).map((i) => ({
    pk: i + 1 + 1000,
    name: `Reservation ${i + 1}`,
    purpose: {
      connect: {
        id: purposes[i % purposes.length].id,
      },
    },
    begin: new Date(
      recurringStart.getFullYear(),
      recurringStart.getMonth(),
      recurringStart.getDate() + i,
      recurringStart.getHours()
    ),
    end: new Date(
      recurringStart.getFullYear(),
      recurringStart.getMonth(),
      recurringStart.getDate() + i,
      recurringStart.getHours() + 2
    ),
  }))
  await prisma.recurringReservation.upsert({
    where: { pk: 1 },
    update: {
      name: 'Recurring Reservation',
      description: 'Test recurring reservation',
      beginTime: '08:00',
      endTime: '10:00',
      beginDate: ((d: Date) =>
        `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(
        recurringStart
      ),
      endDate: ((d: Date) =>
        `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(recurringEnd),
    },
    create: {
      name: 'Recurring Reservation',
      description: 'Test recurring reservation',
      pk: 1,
      beginTime: '08:00',
      endTime: '10:00',
      beginDate: ((d: Date) =>
        `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(
        recurringStart
      ),
      endDate: ((d: Date) =>
        `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`)(recurringEnd),
      reservationUnit: {
        connect: {
          id: ru?.id,
        },
      },
      reservation: {
        create: reservationsForRecurring,
      },
      recuranceInDays: 7,
      weekdays: {
        create: [
          { name: 'Monday', value: 1 },
          { name: 'Tuesday', value: 2 },
          { name: 'Wednesday', value: 3 },
          { name: 'Thursday', value: 4 },
          { name: 'Friday', value: 5 },
          { name: 'Saturday', value: 6 },
          { name: 'Sunday', value: 0 },
        ].map((day) => ({ name: day.name, value: day.value })),
      },
    },
  })
}
