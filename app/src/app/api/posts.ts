// NOTE loading urql before loading urql/core and creating the client
// breaks server components (even the core imports create context)
import { cacheExchange, fetchExchange, createClient } from '@urql/core'
import { GET_POSTS, GET_POST_BY_ID } from '../posts/queries'

const url = 'http://localhost:3000/api/graphql'

const client = createClient({
  url,
  exchanges: [cacheExchange, fetchExchange],
  suspense: true,
})

export const getPostById = (id: string) =>
  client.query(GET_POST_BY_ID, { id }).toPromise()

export const getPosts = () => client.query(GET_POSTS, {}).toPromise()

export const getClient = () => client
