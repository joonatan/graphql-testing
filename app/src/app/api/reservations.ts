// NOTE loading urql before loading urql/core and creating the client
// breaks server components (even the core imports create context)
import { cacheExchange, fetchExchange, createClient } from '@urql/core'
import {
  GET_RESERVATIONS,
  GET_RESERVATION_BY_ID,
} from '../reservations/queries'

// TODO the client should be inside a backend wrapper
const url = 'http://localhost:3000/api/graphql'

const client = createClient({
  url,
  exchanges: [cacheExchange, fetchExchange],
  suspense: true,
})

export const getReservationById = (id: string) =>
  client.query(GET_RESERVATION_BY_ID, { id }).toPromise()

export const getReservations = () =>
  client.query(GET_RESERVATIONS, {}).toPromise()

export const getClient = () => client
