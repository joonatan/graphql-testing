import { getReservationById } from '@/app/api/reservations'
import { ReservationSchema } from '../queries'

export default async function ReservationsPage({
  params,
}: {
  params: { slug: string }
}) {
  const res = await getReservationById(params.slug)
  const { reservation } = ReservationSchema.parse(res.data)

  return (
    <>
      <h1 className="my-12 text-center text-3xl">{reservation.name ?? ''}</h1>
      <div className="">
        <p>{reservation.name}</p>
        <p>{reservation.state}</p>
        <p>{reservation.type}</p>
        <p>{reservation.begin?.toString()}</p>
        <p>{reservation.end?.toString()}</p>
      </div>
    </>
  )
}
