import { gql } from '@urql/core'
import { z } from 'zod'

const ReservationShortSchema = z.object({
  id: z.string(),
  name: z.string().nullish(),
  description: z.string().nullish(),
  // TODO dates
  begin: z.string().pipe(z.coerce.date()).nullish(),
  end: z.string().pipe(z.coerce.date()).nullish(),
  price: z.number().nullish(),
  reserveeName: z.string(),
  // TODO enums
  type: z.string().nullish(),
  state: z.string().nullish(),
  orderStatus: z.string().nullish(),
  reservationUnit: z
    .object({
      id: z.string(),
      name: z.string().nullish(),
      state: z.string().nullish(),
    })
    .nullish(),
})

// Query schemas
export const ReservationSchema = z.object({
  reservation: ReservationShortSchema,
})

export const ReservationsSchema = z.object({
  reservations: z.array(ReservationShortSchema),
})

export type ReservationType = z.infer<typeof ReservationSchema>
export type ReservationShortType = z.infer<typeof ReservationShortSchema>
export type ReservationsType = z.infer<typeof ReservationsSchema>

const COMMON_FRAGMENT = gql`
  fragment CommonReservationFields on Reservation {
    id
    name
    description
    begin
    end
    price
    reserveeName
    type
    state
    orderStatus
    reservationUnit {
      id
      name
      state
    }
  }
`

export const GET_RESERVATIONS = gql`
  ${COMMON_FRAGMENT}
  query Reservations ($where: ReservationWhereInput!) {
    reservations(orderBy: { begin: asc }, where: $where) {
      ...CommonReservationFields
    }
  }
`

export const GET_RESERVATION_BY_ID = gql`
  ${COMMON_FRAGMENT}
  query ($id: ID!) {
    reservation(where: { id: $id }) {
      ...CommonReservationFields
    }
  }
`

