import { getReservations } from '../api/reservations'
import { ReservationsSchema } from './queries'
import ReservationsTable from '../components/ReservationsTable'

export default async function Reservations() {
  const res = await getReservations()
  const { reservations } = ReservationsSchema.parse(res.data)

  return (
    <>
      <h1 className="my-12 text-3xl">Reservations</h1>
      <ReservationsTable baseHref="reservations" reservations={reservations} router="next" />
    </>
  )
}
