'use client'
import React, { type ReactNode } from 'react'
import { default as NextLink } from 'next/link'
import { Link as WouterLink } from 'wouter'

// TODO add a split Link component (with either next / wouter link)
const LinkImpl = ({ router, href, children }: { router: 'next' | 'wouter', href: string, children: ReactNode }) => {
  const cls = "border-b-2 border-transparent hover:border-indigo-600"
  if (router === 'wouter') {
    return (
      <WouterLink href={href} className={cls}>
        {children}
      </WouterLink>
    )
  }

  return (
    <NextLink className={cls} href={href}>
      {children}
    </NextLink>
  )
}

export default LinkImpl

