import Link from 'next/link'
import { PostShortType } from '../posts/queries'

const PostCard = ({ post }: { post: PostShortType }) => (
  <li className="rounded-xl bg-indigo-300 duration-300 hover:bg-indigo-400 dark:bg-indigo-900 dark:hover:bg-indigo-800">
    <Link href={`posts/${post.id}`}>
      <h2 className="p-8">{post.title}</h2>
      <div className="m-2 flex flex-col flex-wrap gap-4">
        {post.tags.map((tag) => (
          <div key={tag.id} className="ml-auto rounded-xl bg-indigo-500 px-4">
            {tag.name}
          </div>
        ))}
      </div>
    </Link>
  </li>
)

export default PostCard
