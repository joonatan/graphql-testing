import React from 'react'
import LinkImpl from '../components/LinkImpl'
import { ReservationShortType } from '../reservations/queries'

const ColStyle = {
  width: '20%',
  maxWidth: '16ch',
}

const ColStyleSmall = {
  width: '10%',
  maxWidth: '16ch',
}

// TODO if it's the same day don't print both dates
const formatTime = (begin: Date, end: Date) => {
  const isSameDay = begin.getDate() === end.getDate() && begin.getMonth() === end.getMonth() && begin.getFullYear() === end.getFullYear()
  const beginTime = begin.toLocaleTimeString(['fi'], {
    year: 'numeric',
    month: 'numeric',
    day: '2-digit',
    hour: 'numeric',
    minute: '2-digit',
  })
  const endTime = end.toLocaleTimeString(['fi'], {
    ...(!isSameDay ? {
      year: 'numeric',
      month: 'numeric',
      day: '2-digit',
      } : {}),
    hour: 'numeric',
    minute: '2-digit',
  })
  return `${beginTime} - ${endTime}`
}

const ReservationsTable = ({
  reservations,
  baseHref,
  router,
}: {
  reservations: ReservationShortType[]
  baseHref: string,
  router: 'next' | 'wouter',
}) => (
  <table className="w-full">
    <thead>
      <tr>
        <th style={ColStyle}>id</th>
        <th style={ColStyle}>name</th>
        <th style={ColStyle}>time</th>
        <th style={ColStyleSmall}>state</th>
        <th style={ColStyleSmall}>type</th>
      </tr>
    </thead>
    <tbody>
      {reservations.map((reservation) => (
        <tr key={reservation.id}>
          <td className="font-mono" style={ColStyle}>
            <LinkImpl href={`${baseHref}/${reservation.id}`} router={router}>
              {reservation.id}
            </LinkImpl>
          </td>
          <td style={ColStyle}>{reservation.name}</td>
          <td className="font-mono" style={ColStyle}>
            {reservation.begin &&
              reservation.end &&
              formatTime(reservation.begin, reservation.end)}
          </td>
          <td style={ColStyleSmall}>{reservation.state}</td>
          <td style={ColStyleSmall}>{reservation.type}</td>
        </tr>
      ))}
    </tbody>
  </table>
)

export default ReservationsTable

