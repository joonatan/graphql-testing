'use client'
import { ReactNode } from 'react'
import { Provider } from 'urql'

const GqlProvider = ({ children }: { children: ReactNode }) => (
  <Provider value={getClient()}>{children}</Provider>
)

export default GqlProvider
