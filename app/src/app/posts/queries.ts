import { gql } from '@urql/core'
import { z } from 'zod'

const DocumentSchema = z.array(
  z.object({
    type: z.string(),
    children: z.array(z.object({ text: z.string() })),
  })
)

const TagsSchema = z.array(
  z.object({
    name: z.string(),
    id: z.string(),
  })
)

export const PostSchema = z.object({
  post: z.object({
    id: z.string(),
    title: z.string(),
    // slug: z.string(),
    tags: TagsSchema,
    content: z.object({
      document: DocumentSchema,
    }),
  }),
})

const PostShortSchema = z.object({
  id: z.string(),
  title: z.string(),
  // slug: z.string(),
  tags: TagsSchema,
})
export const PostsSchema = z.object({
  posts: z.array(PostShortSchema),
})

export type PostType = z.infer<typeof PostSchema>
export type PostShortType = z.infer<typeof PostShortSchema>
export type PostsType = z.infer<typeof PostsSchema>

export const GET_POSTS = gql`
  query {
    posts {
      id
      title
      tags {
        name
        id
      }
    }
  }
`

export const GET_POST_BY_ID = gql`
  query ($id: ID!) {
    post(where: { id: $id }) {
      id
      title
      tags {
        name
        id
      }
      content {
        document
      }
    }
  }
`
