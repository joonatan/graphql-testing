import { getPostById } from '@/app/api/posts'
import { PostSchema } from '../queries'

// Fully server-side rendered page
// implement a client version (refactor the post component to be common)
// client version requires the urql/react and hooks
export default async function PostPage({
  params,
}: {
  params: { slug: string }
}) {
  const res = await getPostById(params.slug)
  const { post } = PostSchema.parse(res.data)

  return (
    <>
      <h1 className="my-12 text-center text-3xl">{post.title ?? 'No post'}</h1>
      <div className="flex w-full justify-end">
        <div className="flex flex-col flex-wrap">
          {post.tags.map((tag) => (
            <div key={tag.id}>{tag.name}</div>
          ))}
        </div>
      </div>
      {post.content.document.map((block) =>
        block.type === 'paragraph' ? (
          <div key={block.type}>
            {block.children.map((child) => (
              <p className="prose mb-12 max-w-prose text-lg" key={child.text}>
                {child.text}
              </p>
            ))}
          </div>
        ) : (
          <div key={block.type}>{block.type} not supported</div>
        )
      )}
    </>
  )
}
