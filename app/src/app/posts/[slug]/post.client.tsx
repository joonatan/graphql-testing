'use client'
import { useQuery } from 'urql'
import { GET_POST_BY_ID, PostSchema } from '../queries'

// FIXME client version isn't used
// TODO refactor the common parts between a server / client components (html)
export default function Post({ slug }: { slug: string }) {
  const [{ data, fetching, error }] = useQuery({
    query: GET_POST_BY_ID,
    variables: { id: slug },
  })

  if (fetching) return <p>Loading...</p>
  if (error) return <p>Oh no... {error.message}</p>

  const { post } = PostSchema.parse(data)

  return (
    <>
      <h1 className="my-12 text-3xl">{post.title ?? 'No post'}</h1>
      <div className="flex w-full justify-end">
        <div className="flex flex-col flex-wrap">
          {post.tags.map((tag) => (
            <div key={tag.id}>{tag.name}</div>
          ))}
        </div>
      </div>
      {post.content.document.map((block) =>
        block.type === 'paragraph' ? (
          <div key={block.type}>
            {block.children.map((child) => (
              <p className="prose mb-12 max-w-prose text-lg" key={child.text}>
                {child.text}
              </p>
            ))}
          </div>
        ) : (
          <div key={block.type}>{block.type} not supported</div>
        )
      )}
    </>
  )
}
