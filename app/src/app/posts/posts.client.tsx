'use client'
import Link from 'next/link'
import { useQuery } from 'urql'
import { GET_POSTS, PostsSchema } from './queries'

// FIXME client version isn't used
// TODO refactor the common parts between a server / client components (html)
export default function PostList() {
  const [result] = useQuery({
    query: GET_POSTS,
  })

  const { data, fetching, error } = result

  if (fetching) return <p>Loading...</p>
  if (error) return <p>Oh no... {error.message}</p>

  const { posts } = PostsSchema.parse(data)

  return (
    <>
      <h1 className="my-12 text-3xl">Posts</h1>
      <ul className="grid grid-cols-2 gap-8 ">
        {posts.map((post) => (
          <li
            key={post.id}
            className="bg-neutral-300 hover:bg-neutral-400 dark:bg-neutral-900 dark:hover:bg-neutral-800"
          >
            <Link href={`posts/${post.id}`}>
              <h2 className="p-8">{post.title}</h2>
              <div className="flex flex-col flex-wrap">
                {post.tags.map((tag) => (
                  <div key={tag.id}>{tag.name}</div>
                ))}
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </>
  )
  // return null
}
