import { getPosts } from '../api/posts'
import PostCard from '../components/PostCard'
import { PostsSchema } from './queries'

export default async function Posts() {
  const res = await getPosts()
  const { posts } = PostsSchema.parse(res.data)

  return (
    <>
      <h1 className="my-12 text-3xl">Posts</h1>
      <ul className="mb-12 grid grid-cols-2 gap-8 ">
        {posts.map((post) => (
          <PostCard key={post.id} post={post} />
        ))}
      </ul>
    </>
  )
}
