/**
 * Pure client side version of the reservations
 * Server side version is in app/src/app/reservations/page.tsx
 * this uses urql provider and hooks to load on the client side
 * the server component version has no client side graphql loading
 */
import Reservations from './reservations.client'

export default async function ReservationsClientPage() {
  return (
    <Reservations />
  )
}
