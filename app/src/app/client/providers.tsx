import { type ReactNode } from 'react'
import { Client, Provider, cacheExchange, fetchExchange } from 'urql'

const client = new Client({
  url: 'http://localhost:3000/api/graphql',
  exchanges: [cacheExchange, fetchExchange],
})

export default function Providers({ children }: { children: ReactNode }) {
  return <Provider value={client}>{children}</Provider>
}

