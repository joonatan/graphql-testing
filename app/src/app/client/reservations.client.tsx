'use client'
import { gql, useQuery } from 'urql'
import { z } from 'zod'
import { Router, Route, Link, Switch, useLocation } from 'wouter'

import {
  GET_RESERVATIONS,
  GET_RESERVATION_BY_ID,
  ReservationSchema,
  ReservationsSchema,
} from '../reservations/queries'
import Providers from './providers'
import ReservationsTable from '../components/ReservationsTable'

const ResUnitBaseSchema = z.object({
    id: z.string(),
    name: z.string(),
    state: z.string().nullable(),
    isDraft: z.boolean(),
})
const ReservationUnitSchema = z.object({
  reservationUnit: z.object({
    minPersons: z.number().nullable(),
    maxPersons: z.number().nullable(),
    maxReservationDuration: z.number().nullable(),
    recurringReservationsCount: z.number(),
    reservationBegins: z.string().nullable(),
    reservationEnds: z.string().nullable(),
    reservationsCount: z.number(),
    reservationUnitType: z.string().nullable(),
    metadataSet: z.object({
      id: z.string().cuid(),
      name: z.string(),
    }).nullable(),
  }).merge(ResUnitBaseSchema),
})

const COMMON_FRAGMENT = gql`
  fragment CommonReservationUnitFields on ReservationUnit {
    id
    name
    state
    isDraft
  }
`

const GET_RESERVATION_UNIT_BY_ID = gql`
  ${COMMON_FRAGMENT}
  query GetReservationUnitById($id: ID!) {
    reservationUnit(where: { id: $id }) {
      ...CommonReservationUnitFields
      minPersons
      maxPersons
      maxReservationDuration
      recurringReservationsCount
      reservationBegins
      reservationEnds
      reservationsCount
      reservationUnitType
      metadataSet {
        id
        name
        requiredFields {
          name
          type
        }
      }
    }
  }
`

const ReservationUnitsSchema = z.object({
  reservationUnits: z.array(ResUnitBaseSchema),
})

const GET_RESERVATION_UNITS = gql`
  ${COMMON_FRAGMENT}
  query GetReservationUnits {
    reservationUnits(where: { isArchived: { equals: false } }) {
      ...CommonReservationUnitFields
    }
  }
`

const BackButton = () => {
  const [location, navigate] = useLocation()

  const handleBackClick = () => {
    navigate(`${location}/..`, { replace: true })
  }

  return (
    <button
      type="button"
      onClick={handleBackClick}
      className="py-2 px-10 rounded-full bg-indigo-400 dark:bg-indigo-900 dark:text-gray-300">
      Back
    </button>
  )
}

const ReservationUnit = ({ id }: { id: string }) => {
  const [{ data, error, fetching }] = useQuery({
    query: GET_RESERVATION_UNIT_BY_ID,
    variables: { id },
  })

  if (fetching) return <p>Loading...</p>
  if (error) return <p>Oh no... {error.message}</p>

  const { reservationUnit } = ReservationUnitSchema.parse(data)

  return (
    <div>
      <BackButton />
      <h2 className="my-12 text-2xl">{reservationUnit.name}</h2>
      <p className="mb-4">state: {reservationUnit.state}</p>
      <p>Max people {reservationUnit.maxPersons}</p>
      <p>Min people {reservationUnit.minPersons}</p>
      {/* TODO add a button to publish / draft */}
      <p>{reservationUnit.isDraft ? 'Draft' : 'Published'}</p>
      {/* TODO these are date strings, print them cleaner */}
      <p>Reservation starts: {reservationUnit.reservationBegins}</p>
      <p>Reservation end: {reservationUnit.reservationEnds}</p>
      <p>{reservationUnit.recurringReservationsCount} recurring reservations</p>
      <p>{reservationUnit.reservationsCount} reservations</p>
      <p>{reservationUnit.metadataSet ? 'metadata should be a link or expand' : 'No metadata, should be button to add it'}</p>
    </div>
  )
}

const Reservation = ({ id }: { id: string }) => {
  const [{ data, error, fetching }] = useQuery({
    query: GET_RESERVATION_BY_ID,
    variables: { id },
  })

  if (fetching) return <p>Loading...</p>
  if (error) return <p>Oh no... {error.message}</p>

  const { reservation } = ReservationSchema.parse(data)

  const beginTime = reservation.begin?.toLocaleTimeString([], {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
  })
  const endTime = reservation.end?.toLocaleTimeString([], {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
  })

  return (
    <div>
      <BackButton />
      <h2 className="my-12 text-2xl">{reservation.name}</h2>
      <p className="mb-4">{`${beginTime} - ${endTime}`}</p>
      <p className="mb-4">state: {reservation.state}</p>
      <p className="mb-4">type: {reservation.type}</p>
      <p className="mb-4">reservee name: {reservation.reserveeName}</p>
      <p className="mb-4">
        reservation unit:
        {reservation.reservationUnit?.id != null ? (
          <Link href={`/reservation-units/${reservation.reservationUnit.id}`} className="underline">
            {reservation.reservationUnit.name ?? 'No reservation unit name'}
          </Link>
        ) : null}
      </p>
    </div>
  )
}

// TODO add filters
const Filters = () => {
  return (
    <div className="mb-8">
      <h2 className="mb-4 text-xl">Filters</h2>
      <p className="prose text-sm">TODO name search</p>
      <p className="prose text-sm">TODO filter the quantity</p>
      <p className="prose text-sm">TODO filter based on state</p>
      <p className="prose text-sm">TODO filter based on type</p>
      <p className="prose text-sm">TODO filter based on reservation unit</p>
      <p className="prose text-sm">
        TODO filter based on unit (parent of a reservation unit)
      </p>
    </div>
  )
}

// TODO add pagination
// TODO add post
// TODO add delete
const Reservations = () => {
  const today = new Date()
  const padToTwo = (n: number) => n < 10 && n >= 0 ? `0${n}` : `${n}`
  const formatDate = (d: Date) =>
    `${d.getFullYear()}-${padToTwo(d.getMonth()+1)}-${padToTwo(d.getDate())}T00:00:00Z`

  const [{ data, error, fetching }] = useQuery({
    query: GET_RESERVATIONS,
    variables: {
      where: { end: { gt: formatDate(today) } },
    },
  })

  if (fetching) return <p>Loading...</p>
  if (error) return <p>Oh no... {error.message}</p>

  const { reservations } = ReservationsSchema.parse(data)

  return (
    <>
      <Link href="/reservation-units" className="underline">
        to Reservation Units
      </Link>
      <Filters />
      <h2 className="mb-4 text-xl">Reservations ({reservations.length})</h2>
      <ReservationsTable baseHref="/reservations" reservations={reservations} router="wouter" />
    </>
  )
}


const ReservationUnits = () => {
  const [{ data, error, fetching }] = useQuery({ query: GET_RESERVATION_UNITS })

  if (fetching) return <p>Loading...</p>
  if (error) return <p>Oh no... {error.message}</p>

  const { reservationUnits } = ReservationUnitsSchema.parse(data)

  return (
    <>
      <h2 className="my-12 text-2xl">Reservation Units</h2>
      <ul>
        {reservationUnits.map((x) => (
          <li key={x.id}>
            <Link href={`/reservation-units/${x.id}`} className="underline">
              {x.name}
            </Link>
          </li>
        ))}
      </ul>
    </>
  )
}

export default function ReservationsWrapped() {
  return (
    <Providers>
      <Router base="/client">
        <h1 className="my-12 text-3xl">Reservations Client</h1>
        <Route path="/">
          <Reservations />
        </Route>
        <Route path="/reservations/:id">
          {(params) => <Reservation id={params.id} />}
        </Route>
        <Switch>
          <Route path="/reservation-units/:id">
            {(params) => <ReservationUnit id={params.id} />}
          </Route>
          <Route path="/reservation-units">
            <ReservationUnits />
          </Route>
        </Switch>
      </Router>
    </Providers>
  )
}

