module.exports = {
  extends: [
    'prettier',
    'eslint:recommended',
    // 'plugin:@typescript-eslint/recommended',
    'plugin:tailwindcss/recommended',
    'next/core-web-vitals',
  ],
  parser: '@typescript-eslint/parser',
  plugins: [
    //'@typescript-eslint',
    'prettier',
    'tailwindcss',
  ],
  rules: {
    // '@typescript-eslint/consistent-type-imports': 'warn',
    // '@typescript-eslint/no-unused-vars': 'warn',
  },
  env: {
    browser: true,
    es2021: true,
  },
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
  },
}
